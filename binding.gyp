{
    "targets": [
        {
            "target_name": "pokerlib",
            "cflags_cc": ["-fexceptions"],
            "cflags_cc!": ["-fno-rtti"],
            "sources": ["src/libpoker.cc", "src/evaluator.cc", "src/hand_distribution.cc", "src/equity.cc"],
            "include_dirs": [
                "include"
            ]
        }
    ]
}
