/**
 * \file evaluator.h
 * \copyright (c) 2013 Atte Backman. All rights reserved.
 * \author Atte Backman
 */

#ifndef __EVALUATOR_H
#define __EVALUATOR_H

#include <node.h>

/**
 * \class Evaluator
 *
 */
class Evaluator: public node::ObjectWrap
{
    public:
        /**
         *
         *
         */
        static void Init(v8::Handle<v8::Object> exports);

    public:
        int *_hand_ranks;

    private:
        Evaluator();
        ~Evaluator();

        static v8::Handle<v8::Value> New(const v8::Arguments &args);
        static v8::Handle<v8::Value> Load(const v8::Arguments &args);
        static v8::Handle<v8::Value> Verify(const v8::Arguments &args);

    private:
        int64_t GenerateID(int64_t id, int new_card);
        int SaveID(int64_t id);
        unsigned Find(unsigned u);
        int Evaluation(int64_t id);
        int Evaluate5Hand(int *hand);
        int Evaluate6Hand(int *hand);
        int Evaluate7Hand(int *hand);

    private:
        int64_t *_ids;
        int64_t _max_id;
        int _n_cards;
        int _n_id;
        int _max_hand_rank;
};

#endif
