#ifndef __HAND_DISTRIBUTION_H
#define __HAND_DISTRIBUTION_H

#include <string>
#include <vector>
#include <inttypes.h>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/shared_ptr.hpp>
#include <node.h>

class HandDistribution: public node::ObjectWrap
{
    public:
        typedef boost::shared_ptr<HandDistribution> ptr;
        typedef std::vector<int64_t>::iterator iter;

    public:
        HandDistribution(const std::string &s = "", int64_t dc = 0);
        ~HandDistribution();

        static void Init(v8::Handle<v8::Object> exports);

        int64_t Choose(uint64_t &used_cards);
        uint32_t NumHands() const;

    private:

        static v8::Handle<v8::Value> Generate(const v8::Arguments &args);
        static v8::Handle<v8::Value> New(const v8::Arguments &args);
        static v8::Handle<v8::Value> GetHandsAmount(const v8::Arguments &args);
        static v8::Handle<v8::Value> GetNotation(const v8::Arguments &args);
        static v8::Handle<v8::Value> GetHands(const v8::Arguments &args);
        static v8::Handle<v8::Value> GetEquity(const v8::Arguments &args);
        static v8::Handle<v8::Value> IsGood(const v8::Arguments &args);

    private:
        bool Instantiate(const std::string &n, int64_t dead_cards);

        bool StringToBitmask(int64_t &m, const std::string &s);
        int32_t CharToRank(char c);
        int32_t CharToSuit(char s);
        void AddHand(unsigned int r1, unsigned int s1, unsigned int r2, unsigned int s2, int64_t dead_cards);
        void AddHand(int64_t c1, int64_t c2, int64_t dead_cards);
        void ToIndex(unsigned int &m, unsigned int r, unsigned int s);
        bool IsColliding(unsigned int r, unsigned int s, int64_t dead_cards);
        bool IsColliding(int64_t c, int64_t dead_cards);
        std::string IndexToString(int32_t index);
        std::string HandToString(int64_t h);

    private:
        std::vector<int64_t> _hands;
        bool _is_unary;
        std::string _notation;

    public:
        uint32_t _rank;
        uint32_t _wins;
        float _ties;
        float _equity;
        uint64_t _picked_hand;
        bool _is_good;
        HandDistribution *_next;
        boost::random::uniform_int_distribution<> _random_hand;
        boost::random::mt19937 _rng;
};

#endif
