#ifndef __EQUITY_H_
#define __EQUITY_H_

#include <vector>
#include <inttypes.h>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <node.h>
#include "hand_distribution.h"
#include "evaluator.h"

using namespace boost;

class Equity: public node::ObjectWrap
{
    public:
        typedef struct AsyncData {
            v8::Persistent<v8::Function> callback;
            Equity *obj;
        } AsyncData;

        static boost::random::mt19937 _rng;
        static boost::random::uniform_int_distribution<> _random_seed;
        static uv_async_t _async;

    public:
        static void Init(v8::Handle<v8::Object> exports);

    private:
        Equity();

        static v8::Handle<v8::Value> New(const v8::Arguments &args);
        static v8::Handle<v8::Value> MonteCarlo(const v8::Arguments &args);
        static v8::Handle<v8::Value> AddHandDistribution(const v8::Arguments &args);
        static v8::Handle<v8::Value> SetEvaluator(const v8::Arguments &args);

        static void MonteCarloWorker(uv_work_t *req);
        static void MonteCarloAfter(uv_work_t *req, int status);
        static void MonteCarloProgress(uv_async_t *handle, int status);

        uint32_t Combinations(uint32_t n, uint32_t r);
        uint16_t BitCount(uint64_t n);
        uint32_t EnumerateBoards(shared_array<uint64_t> &p);
        void LinkHandDistributions();

        static unsigned long long rdtsc(void);

    private:
        std::vector<shared_ptr<HandDistribution> > _hand_distributions;
        shared_array<uint64_t> _boards;
        uint32_t _boards_amount;
        uint64_t _board_cards;
        uint64_t _dead_cards;
        uint64_t _essential_cards;
        Evaluator *_evaluator;
        double _time_elapsed;

        uint64_t _current_trial;
        uint64_t _total_trials;
};

#endif

#if defined(__i386__)

__inline__ unsigned long long Equity::rdtsc(void)
{
    unsigned long long int x;

    __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));

    return x;
}

#elif defined(__x86_64__)

__inline__ unsigned long long Equity::rdtsc(void)
{
    unsigned hi, lo;

    __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));

    return ((unsigned long long)lo) | (((unsigned long long)hi) << 32);
}

#endif
