pokerlib = require './../build/Release/pokerlib'
colors = require 'colors'
equity = new pokerlib.Equity()
evaluator = new pokerlib.Evaluator()

evaluator.Load()

hand_distributions = []

for range in ['AA', 'KK', 'QQ', 'JJ', 'TT+']
    r = new pokerlib.HandDistribution range
    hand_distributions.push r
    equity.AddHandDistribution r

equity.SetEvaluator evaluator

equity.MonteCarlo (result) ->
    console.log "Done! Evaluated #{Math.round result} hands/s\n".yellow

    for hand_distribution, i in hand_distributions
        console.log "Hand range ##{i + 1}:".underline.green
        console.log "    Range : #{hand_distribution.GetNotation()} (#{hand_distribution.GetHandsAmount()} hands)"
        console.log "    Equity: #{(hand_distribution.GetEquity().toPrecision 4)}%\n"

    return

