#include <node.h>
#include "evaluator.h"
#include "hand_distribution.h"
#include "equity.h"

using namespace v8;

void InitAll(Handle<Object> exports)
{
    Evaluator::Init(exports);
    HandDistribution::Init(exports);
    Equity::Init(exports);
}

NODE_MODULE(pokerlib, InitAll)
