/**
 *
 * \author Atte Backman <koodiapina@live.com>
 * \file hand_distribution.cc
 */

#include <ctime>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <sys/types.h>
#include <unistd.h>
#include "hand_distribution.h"

using namespace boost;
using namespace v8;

HandDistribution::HandDistribution(const std::string &s, int64_t dc)
: _rng(std::time(0) + getpid())
, _is_good(true)
{
    if (s.length() > 0)
        _is_good = Instantiate(s, dc);
}

HandDistribution::~HandDistribution()
{
}

void HandDistribution::Init(Handle<Object> exports)
{
    Local<FunctionTemplate> tpl = FunctionTemplate::New(New);

    tpl->SetClassName(String::NewSymbol("HandDistribution"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    tpl->PrototypeTemplate()->Set(String::NewSymbol("Generate"), FunctionTemplate::New(Generate)->GetFunction());
    tpl->PrototypeTemplate()->Set(String::NewSymbol("GetHandsAmount"), FunctionTemplate::New(GetHandsAmount)->GetFunction());
    tpl->PrototypeTemplate()->Set(String::NewSymbol("GetHands"), FunctionTemplate::New(GetHands)->GetFunction());
    tpl->PrototypeTemplate()->Set(String::NewSymbol("GetEquity"), FunctionTemplate::New(GetEquity)->GetFunction());
    tpl->PrototypeTemplate()->Set(String::NewSymbol("GetNotation"), FunctionTemplate::New(GetNotation)->GetFunction());
    tpl->PrototypeTemplate()->Set(String::NewSymbol("IsGood"), FunctionTemplate::New(IsGood)->GetFunction());

    Persistent<Function> constructor = Persistent<Function>::New(tpl->GetFunction());

    exports->Set(String::NewSymbol("HandDistribution"), constructor);
}

Handle<Value> HandDistribution::New(const Arguments &args)
{
    HandleScope scope;

    std::string param1(*v8::String::Utf8Value(args[0]->ToString()));
    Local<Number> v2 = Local<Number>::Cast(args[1]);
    HandDistribution *obj;

    if (param1.length() > 1)
        obj = new HandDistribution(param1, v2->Value());
    else
        obj = new HandDistribution();

    obj->Wrap(args.This());

    return args.This();
}

Handle<Value> HandDistribution::IsGood(const Arguments &args)
{
    HandleScope scope;

    HandDistribution *obj = ObjectWrap::Unwrap<HandDistribution>(args.This());

    return scope.Close(Boolean::New(obj->_is_good));
}

Handle<Value> HandDistribution::Generate(const Arguments &args)
{
    HandleScope scope;

    HandDistribution *obj = ObjectWrap::Unwrap<HandDistribution>(args.This());

    std::string param1(*v8::String::Utf8Value(args[0]->ToString()));
    Local<Number> v2 = Local<Number>::Cast(args[1]);
    Local<Function> cb = Local<Function>::Cast(args[2]);

    obj->Instantiate(param1, (int64_t)v2->Value());

    Local<Value> argv[1];

    cb->Call(Context::GetCurrent()->Global(), 0, argv);

    return scope.Close(Undefined());
}

Handle<Value> HandDistribution::GetHandsAmount(const Arguments &args)
{
    HandleScope scope;

    HandDistribution *obj = ObjectWrap::Unwrap<HandDistribution>(args.This());

    return scope.Close(Number::New(obj->_hands.size()));
}

Handle<Value> HandDistribution::GetNotation(const Arguments &args)
{
    HandleScope scope;

    return scope.Close(String::New(ObjectWrap::Unwrap<HandDistribution>(args.This())->_notation.c_str()));
}

Handle<Value> HandDistribution::GetHands(const Arguments &args)
{
    HandleScope scope;

    HandDistribution *obj = ObjectWrap::Unwrap<HandDistribution>(args.This());

    Handle<Array> result = Array::New(obj->_hands.size());

    for (size_t i = 0; i < obj->_hands.size(); i++)
        result->Set(i, Local<Value>::New(String::New(obj->HandToString(obj->_hands[i]).c_str())));

    return scope.Close(result);
}

uint32_t HandDistribution::NumHands() const
{
    return _hands.size();
}

Handle<Value> HandDistribution::GetEquity(const Arguments &args)
{
    HandleScope scope;

    return scope.Close(Number::New(ObjectWrap::Unwrap<HandDistribution>(args.This())->_equity));
}

bool HandDistribution::Instantiate(const std::string &n, int64_t dead_cards)
{
    for (split_iterator<std::string::const_iterator> r = make_split_iterator(n, first_finder(",", is_iequal()));
         r != split_iterator<std::string::const_iterator>();
         ++r) {
        std::string range = copy_range<std::string>(*r);

        trim(range);

        if (range.length() < 2)
            return false;

        _notation += range;
        _notation += ", ";

        bool is_plus = find_first(range, "+");
        int64_t c1;

        if (StringToBitmask(c1, range.substr(1, 2))) { // E.g. A2h?
            int32_t r1 = CharToRank(range[0]), r2 = CharToRank(range[1]);

            if (r1 != -1) {
                int32_t s2 = CharToSuit(range[2]);

                if (!is_plus || r2 == 14) {
                     for (unsigned int s1 = 0; s1 < 4; ++s1)
                         AddHand(r1, s1, r2, s2, dead_cards);
                } else {
                    for (unsigned int s1 = 0; s1 < 4; ++s1)
                        if (r2 > r1)
                            for (unsigned int i = r2; i < 15; ++i)
                                AddHand(r1, s1, i, s2, dead_cards);
                        else for (unsigned int i = r2; i < static_cast<unsigned int>(r1); ++i)
                            AddHand(r1, s1, i, s2, dead_cards);
                }
            } else
                return false;
        } else if (StringToBitmask(c1, range.substr(0, 2))) { // Possibly a unary hand...
            int r1 = CharToRank(range[0]), r2 = CharToRank(range[2]);

            if (r2 != -1) {
                if (range.length() > 5)
                    return false; // With specific hand, the size can be 5 at most (e.g. Ah2d+).

                int64_t c2;
                bool c2ok = StringToBitmask(c2, range.substr(2, 2));

                if (range.length() == 5) {
                    if (range[4] != '+' || !c2ok)
                        return false; // + is the only allowed letter prior to e.g. AhKh.

                    unsigned int s1 = CharToSuit(range[1]), s2 = CharToSuit(range[3]);

                    for (unsigned int r = r2; r < 15; ++r)
                        AddHand(r1, s1, r, s2, dead_cards);
                } else {
                    if (c2ok) {
                        AddHand(c1, c2, dead_cards);

                        _is_unary = true;
                    } else {
                        // e.g. AhJ.

                        unsigned int s1 = CharToSuit(range[1]);

                        for (unsigned int s2 = 0; s2 < 4; ++s2)
                            if (is_plus) {
                                if (r2 > r1)
                                    for (unsigned int i = r2; i < 15; ++i)
                                        AddHand(r1, s1, i, s2, dead_cards);
                                    else for (unsigned int i = r2; i < static_cast<unsigned int>(r1); ++i)
                                        AddHand(r1, s1, i, s2, dead_cards);
                            } else AddHand(r1, s1, r2, s2, dead_cards);
                    }
                }
            } else return false;
        } else { // Wasn't a unary hand or e.g. AhK or AhK+.
            int r1 = CharToRank(range[0]), r2 = CharToRank(range[1]);

            if (r1 < 0 || r2 < 0)
                return false;

            if (r2 > r1) {
                r2 = r1 ^ r2;
                r1 = r2 ^ r1;
                r2 = r1 ^ r2;
            }

            bool is_offsuited = find_first(range, "O"), is_suited = find_first(range, "S");

            if (range[0] == range[1]) { // A pair?
                bool is_slice = find_first(range, "-");
                int r3;

                if (is_slice) {
                    if (range.length() != 5) // We cannot have '+-so' with a slice; no reason.
                        return false;

                    r3 = CharToRank(range[3]);

                    if (r3 < 0)
                        return false;

                    if (r3 < r1) {
                        r3 = r1 ^ r3;
                        r1 = r3 ^ r1;
                        r3 = r1 ^ r3;
                    }

                    ++r3;
                } else if (is_suited || is_offsuited) // Only two ranks, nothing more.
                    return false;

                for (int s1 = 0; s1 < 4; ++s1)
                    for (int s2 = s1 + 1; s2 < 4; ++s2)
                        if (is_plus)
                            for (int i = r1; i < 15; ++i)
                                AddHand(i, s1, i, s2, dead_cards);
                        else if (is_slice)
                            for (int i = r1; i < r3; ++i)
                                AddHand(i, s1, i, s2, dead_cards);
                        else
                            AddHand(r1, s1, r1, s2, dead_cards);
            } else for (int i = r2; i < r1; ++i) { // Was not a pair...
                if (is_suited)
                    for (int s1 = 0; s1 < 4; ++s1)
                        AddHand(r1, s1, i, s1, dead_cards);
                else for (int s1 = 0; s1 < 4; ++s1)
                    for (int s2 = 0; s2 < 4; ++s2)
                        if (is_offsuited) {
                            if (s1 != s2)
                                AddHand(r1, s1, i, s2, dead_cards);
                        } else AddHand(r1, s1, i, s2, dead_cards);

                    if (!is_plus)
                        break;
            }
        }
    }

    trim_right_if(_notation, is_any_of(",- "));

    _random_hand = random::uniform_int_distribution<>(0, _hands.size() - 1);

    return true;
}

bool HandDistribution::StringToBitmask(int64_t &m, const std::string &s)
{
    int32_t r = CharToRank(s[0]), su = CharToSuit(s[1]);

    if (r < 0 || su < 0)
        return false;

    m = static_cast<int64_t>(1) << (((r - 2) << 2) + su + 1);

    return true;
}

int32_t HandDistribution::CharToRank(char c)
{
    switch (c) {
        case 'A': return 14;
        case 'K': return 13;
        case 'Q': return 12;
        case 'J': return 11;
        case 'T': return 10;
    }

    try {
        short n = lexical_cast<short>(c);

        return ((n < 2) || (n > 9)) ? -1 : n;
    } catch (bad_lexical_cast &) {
        return -1;
    }
}

int32_t HandDistribution::CharToSuit(char s)
{
    switch (s) {
        case 's': return 3;
        case 'd': return 2;
        case 'c': return 1;
        case 'h': return 0;
    }

    return -1;
}

void HandDistribution::AddHand(unsigned int r1, unsigned int s1, unsigned int r2, unsigned int s2, int64_t dead_cards)
{
    if ((!r1 && !s1) || (!r2 && !s2))
        return;

    if ((r1 == r2) && (s1 == s2))
        return;

    if (!IsColliding(r1, s1, dead_cards) && !IsColliding(r2, s2, dead_cards)) {
        unsigned int m[2];

        ToIndex(m[0], r1, s1);
        ToIndex(m[1], r2, s2);

        for (std::vector<int64_t>::iterator h = _hands.begin(); h != _hands.end(); ++h)
            if ((((static_cast<int64_t>(1) << m[0]) & *h) != 0) && (((static_cast<int64_t>(1) << m[1]) & *h) != 0))
                return;

        _hands.push_back((static_cast<int64_t>(1) << m[0]) | (static_cast<int64_t>(1) << m[1]));
    }
}

void HandDistribution::AddHand(int64_t c1, int64_t c2, int64_t dead_cards)
{
    if (c1 == c2)
        return;

    if (!IsColliding(c1, dead_cards) && !IsColliding(c2, dead_cards)) {
        for (std::vector<int64_t>::iterator h = _hands.begin(); h != _hands.end(); ++h)
            if ((((c1 & *h) != 0) && ((c2 & *h) != 0)) || (!c1 || !c2))
                return;

        _hands.push_back(c1 | c2);
    }
}

void HandDistribution::ToIndex(unsigned int &m, unsigned int r, unsigned int s)
{
    m = ((r - 2) << 2) + s + 1;
}

bool HandDistribution::IsColliding(unsigned int r, unsigned int s, int64_t dead_cards)
{
    unsigned int m;

    ToIndex(m, r, s);

    return ((static_cast<uint64_t>(1) << m) & dead_cards) != 0;
}

bool HandDistribution::IsColliding(int64_t c, int64_t dead_cards)
{
    return (c & dead_cards) != 0;
}

std::string HandDistribution::IndexToString(int32_t index)
{
    switch (index) {
        case 1: return "2h";
        case 2: return "2c";
        case 3: return "2d";
        case 4: return "2s";
        case 5: return "3h";
        case 6: return "3c";
        case 7: return "3d";
        case 8: return "3s";
        case 9: return "4h";
        case 10: return "4c";
        case 11: return "4d";
        case 12: return "4s";
        case 13: return "5h";
        case 14: return "5c";
        case 15: return "5d";
        case 16: return "5s";
        case 17: return "6h";
        case 18: return "6c";
        case 19: return "6d";
        case 20: return "6s";
        case 21: return "7h";
        case 22: return "7c";
        case 23: return "7d";
        case 24: return "7s";
        case 25: return "8h";
        case 26: return "8c";
        case 27: return "8d";
        case 28: return "8s";
        case 29: return "9h";
        case 30: return "9c";
        case 31: return "9d";
        case 32: return "9s";
        case 33: return "Th";
        case 34: return "Tc";
        case 35: return "Td";
        case 36: return "Ts";
        case 37: return "Jh";
        case 38: return "Jc";
        case 39: return "Jd";
        case 40: return "Js";
        case 41: return "Qh";
        case 42: return "Qc";
        case 43: return "Qd";
        case 44: return "Qs";
        case 45: return "Kh";
        case 46: return "Kc";
        case 47: return "Kd";
        case 48: return "Ks";
        case 49: return "Ah";
        case 50: return "Ac";
        case 51: return "Ad";
        case 52: return "As";
    }

    return std::string("na");
}

std::string HandDistribution::HandToString(int64_t h)
{
    std::string s;

    for (int i = 52; i > -1; --i)
        if (((h >> i) & 1) == 1)
            s += IndexToString(i);

    return s;
}

int64_t HandDistribution::Choose(uint64_t &used_cards)
{
    _picked_hand = _hands[_random_hand(_rng)];

    if ((used_cards & _picked_hand) != 0)
        return 0;

    used_cards |= _picked_hand;

    return _picked_hand;
}
