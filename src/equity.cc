#include <ctime>
#include <boost/timer.hpp>
#include <algorithm>
#include <cstring>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include "equity.h"

#define BSFQ(b, x) asm("bsfq %0, %0" : "=r" (x) : "0" (b)); \
    cb &= cb - 1;

using namespace boost;
using namespace v8;

random::mt19937 Equity::_rng = random::mt19937(std::time(0));
random::uniform_int_distribution<> Equity::_random_seed = random::uniform_int_distribution<>(0, 999999);
uv_async_t Equity::_async;

Equity::Equity()
: _board_cards(0)
, _dead_cards(0)
, _essential_cards(0)
{
}

void Equity::Init(Handle<Object> exports)
{
    Local<FunctionTemplate> tpl = FunctionTemplate::New(New);

    tpl->SetClassName(String::NewSymbol("Equity"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    tpl->PrototypeTemplate()->Set(String::NewSymbol("MonteCarlo"), FunctionTemplate::New(MonteCarlo)->GetFunction());
    tpl->PrototypeTemplate()->Set(String::NewSymbol("AddHandDistribution"), FunctionTemplate::New(AddHandDistribution)->GetFunction());
    tpl->PrototypeTemplate()->Set(String::NewSymbol("SetEvaluator"), FunctionTemplate::New(SetEvaluator)->GetFunction());

    Persistent<Function> constructor = Persistent<Function>::New(tpl->GetFunction());

    exports->Set(String::NewSymbol("Equity"), constructor);
}

Handle<Value> Equity::New(const Arguments &args)
{
    HandleScope scope;

    Equity *obj = new Equity();

    obj->Wrap(args.This());

    return args.This();
}

Handle<Value> Equity::MonteCarlo(const Arguments &args)
{
    HandleScope scope;

    uv_work_t *req = new uv_work_t;
    AsyncData *async_data = new AsyncData;

    req->data = async_data;

    async_data->callback = Persistent<Function>::New(Local<Function>::Cast(args[0]));
    async_data->obj = ObjectWrap::Unwrap<Equity>(args.This());

    uv_async_init(uv_default_loop(), &_async, MonteCarloProgress);
    uv_queue_work(uv_default_loop(), req, MonteCarloWorker, MonteCarloAfter);

    return scope.Close(Undefined());
}

void Equity::MonteCarloProgress(uv_async_t *handle, int status)
{
}

Handle<Value> Equity::AddHandDistribution(const Arguments &args)
{
    HandleScope scope;

    Equity *obj = ObjectWrap::Unwrap<Equity>(args.This());

    obj->_hand_distributions.push_back(HandDistribution::ptr(node::ObjectWrap::Unwrap<HandDistribution>(args[0]->ToObject())));
    obj->_hand_distributions.back()->_rng = random::mt19937(_random_seed(_rng) + getpid());

    return scope.Close(Undefined());
}

Handle<Value> Equity::SetEvaluator(const Arguments &args)
{
    HandleScope scope;

    Equity *obj = ObjectWrap::Unwrap<Equity>(args.This());

    obj->_evaluator = node::ObjectWrap::Unwrap<Evaluator>(args[0]->ToObject());

    return scope.Close(Undefined());
}

void Equity::MonteCarloAfter(uv_work_t *req, int status)
{
    HandleScope scope;

    AsyncData *async_data = (AsyncData *)req->data;
    Equity *obj = async_data->obj;
 // Handle<Array> result = Array::New(async_data->obj->_hand_distributions.size());

    for (size_t i = 0; i < obj->_hand_distributions.size(); i++) {
        float r = (static_cast<float>(obj->_hand_distributions[i]->_wins) + obj->_hand_distributions[i]->_ties) / obj->_current_trial * 100.0;

        obj->_hand_distributions[i]->_equity = r;

     // result->Set(i, Local<Value>::New(Number::New(r)));
    }

    Handle<Value> argv[] = {
     // result
        Number::New(obj->_current_trial / obj->_time_elapsed)
    };

    async_data->callback->Call(Context::GetCurrent()->Global(), 1, argv);
    async_data->callback.Dispose();

    uv_close((uv_handle_t*) &_async, NULL);

    delete async_data;
    delete req;
}

void Equity::MonteCarloWorker(uv_work_t *req)
{
    AsyncData *async_data = (AsyncData *)req->data;
    Equity *obj = async_data->obj;

    for (std::vector<shared_ptr<HandDistribution> >::iterator d = obj->_hand_distributions.begin(); d != obj->_hand_distributions.end(); ++d) {
        (*d)->_equity = 0.0;
        (*d)->_wins = 0;
        (*d)->_ties = 0.0;
        (*d)->_rank = 0;
    }

    obj->_boards_amount = obj->EnumerateBoards(obj->_boards);
    obj->_time_elapsed = 0.0;
    obj->_current_trial = 0;

    random::mt19937 rng(std::time(0));
    random::uniform_int_distribution<> random_board(0, obj->_boards_amount - 1);

    obj->LinkHandDistributions();

    HandDistribution *hd_first = obj->_hand_distributions[0].get(), *hd_current = hd_first, *hd_winner = NULL;
    uint64_t current_board = 0;

    timer t;
    timespec ts, ts2;
int cl = 0;

    clock_gettime(CLOCK_REALTIME, &ts);

    do {
        hd_current = hd_first;

        if (!(obj->_current_trial & 0xffff)) {
            clock_gettime(CLOCK_REALTIME, &ts2);

            if (ts2.tv_sec != ts.tv_sec) {
                ++cl;

                ts.tv_sec = ts2.tv_sec;
                _async.data = (void *)&obj->_current_trial;

                uv_async_send(&_async);
            }
        }

        uint64_t cards_used = 0, pocket, board;

        do {
            if ((pocket = hd_current->Choose(cards_used)) == 0)
                break;
        } while ((hd_current = hd_current->_next) != hd_first);

        if (pocket == 0)
            continue;

        do {
            board = obj->_boards[current_board++];

            if (current_board == obj->_boards_amount + 1)
                current_board = 0;
        } while ((board & cards_used) != 0);

        cards_used |= board;

        uint64_t b = 0;
        unsigned int p, best = 0, s = 1;

        do {
            int64_t cb = hd_current->_picked_hand |= board;

            BSFQ(cb, b)

            p = obj->_evaluator->_hand_ranks[53 + b];

            while (cb) {
                BSFQ(cb, b)

                p = obj->_evaluator->_hand_ranks[p + b];
            }

            hd_current->_rank = p;

            if (p > best) {
                hd_winner = hd_current;
                best = p;
                s = 1;
            } else if (p == best)
                ++s;

            hd_current = hd_current->_next;
        } while (hd_current != hd_first);

        ++obj->_current_trial;

        if (s == 1)
            ++hd_winner->_wins;
        else {
            float t = 1.0 / s;

            for (std::vector<shared_ptr<HandDistribution> >::iterator d = obj->_hand_distributions.begin(); d != obj->_hand_distributions.end(); ++d)
                if ((*d)->_rank == best)
                    (*d)->_ties += t;
        }

        hd_first = hd_current->_next;
    } while (cl < 5);

    obj->_time_elapsed = t.elapsed();
}

void Equity::LinkHandDistributions()
{
    uint32_t c = _hand_distributions.size();

    if (c == 0)
        return;

    for (uint32_t i = 0; i < c; ++i)
        if (i < c - 1)
            _hand_distributions[i]->_next = _hand_distributions[i + 1].get();

    _hand_distributions[c - 1]->_next = _hand_distributions[0].get(); // Circular linked list.
}

uint32_t Equity::Combinations(uint32_t n, uint32_t r)
{
    if (r > n || r == 0)
        return 1;

    uint32_t v = n--;

    for (uint32_t i = 2; i < r + 1; ++i, --n)
        v = v * n / i;

    return v;
}

uint16_t Equity::BitCount(uint64_t n)
{
    uint16_t c = 0;

    while (n) {
        c += n & 0x1u;
        n >>= 1;
    }

    return c;
}

uint32_t Equity::EnumerateBoards(shared_array<uint64_t> &p)
{
    uint32_t cards_to_draw = 5 - BitCount(_board_cards), cto = 49 + (5 - cards_to_draw), count = 0;
    uint32_t boards_tally = Combinations(BitCount(0xfffffffffffff ^ (_dead_cards | _board_cards | _essential_cards)), cards_to_draw);

    p = shared_array<uint64_t>(new uint64_t[boards_tally]);

    memset(p.get(), 0, sizeof(uint64_t) * boards_tally);

    if (cards_to_draw > 0)
        for (uint32_t c1 = 1; c1 < cto; ++c1)
            if (cards_to_draw > 1)
                for (uint32_t c2 = c1 + 1; c2 < cto + 1; ++c2)
                    if (cards_to_draw > 2)
                        for (uint32_t c3 = c2 + 1; c3 < cto + 2; ++c3)
                            if (cards_to_draw > 3)
                                for (uint32_t c4 = c3 + 1; c4 < cto + 3; ++c4)
                                    if (cards_to_draw > 4)
                                        for (uint32_t c5 = c4 + 1; c5 < 53; ++c5) {
                                            uint64_t b = static_cast<uint64_t>(1) << c1
                                                       | static_cast<uint64_t>(1) << c2
                                                       | static_cast<uint64_t>(1) << c3
                                                       | static_cast<uint64_t>(1) << c4
                                                       | static_cast<uint64_t>(1) << c5;

                                            if ((b & (_dead_cards | _board_cards | _essential_cards)) == 0) {
                                                p[count] = b;

                                                ++count;
                                            }
                                        }
                                    else {
                                        uint64_t b = static_cast<uint64_t>(1) << c1
                                                   | static_cast<uint64_t>(1) << c2
                                                   | static_cast<uint64_t>(1) << c3
                                                   | static_cast<uint64_t>(1) << c4;

                                        if ((b & (_dead_cards | _board_cards | _essential_cards)) == 0) {
                                            p[count] = b;

                                            ++count;
                                        }
                                    }
                            else {
                                uint64_t b = static_cast<uint64_t>(1) << c1
                                           | static_cast<uint64_t>(1) << c2
                                           | static_cast<uint64_t>(1) << c3;

                                if ((b & (_dead_cards | _board_cards | _essential_cards)) == 0) {
                                    p[count] = b;

                                    ++count;
                                }
                            }
                    else {
                        uint64_t b = static_cast<uint64_t>(1) << c1
                                   | static_cast<uint64_t>(1) << c2;

                        if ((b & (_dead_cards | _board_cards | _essential_cards)) == 0) {
                            p[count] = b;

                            ++count;
                        }
                    }
            else {
                uint64_t b = static_cast<uint64_t>(1) << c1;

                if ((b & (_dead_cards | _board_cards | _essential_cards)) == 0) {
                    p[count] = b;

                    ++count;
                }
            }
    else {
        p[0] = _board_cards;
        count = 1;
    }

    std::random_shuffle(&p[0], &p[count]);

    return count;
}
