#include <node.h>
#include <cstring>
#include <fstream>
#include "evaluator.h"
#include "hash.h"

#define CHECK() q = Evaluate5Hand(hand); if (q < best) best = q;

#define PERMUTATION6(x) hand[0] = t_hand[GPERMUTATION6[x][0]]; \
    hand[1] = t_hand[GPERMUTATION6[x][1]]; \
    hand[2] = t_hand[GPERMUTATION6[x][2]]; \
    hand[3] = t_hand[GPERMUTATION6[x][3]]; \
    hand[4] = t_hand[GPERMUTATION6[x][4]]; \
    CHECK()

#define PERMUTATION7(x) hand[0] = t_hand[GPERMUTATION7[x][0]]; \
    hand[1] = t_hand[GPERMUTATION7[x][1]]; \
    hand[2] = t_hand[GPERMUTATION7[x][2]]; \
    hand[3] = t_hand[GPERMUTATION7[x][3]]; \
    hand[4] = t_hand[GPERMUTATION7[x][4]]; \
    CHECK()

#define SWAP(x, y) x[0] = y[0]; \
    x[1] = y[1]; \
    x[2] = y[2]; \
    x[3] = y[3]; \
    x[4] = y[4]; \
    x[5] = y[5]; \
    x[6] = y[6]; \
    x[7] = y[7];

#define _SWAP(I, J) \
{ \
    if (work_cards[I] < work_cards[J]) { \
        work_cards[I] ^= work_cards[J]; \
        work_cards[J] ^= work_cards[I]; \
        work_cards[I] ^= work_cards[J]; \
    } \
}

using namespace v8;

Evaluator::Evaluator()
: _hand_ranks(NULL)
, _ids(NULL)
, _max_id(0)
, _n_cards(0)
, _n_id(1)
, _max_hand_rank(0)
{
    _hand_ranks = new int[32487834];
}

Evaluator::~Evaluator()
{
    delete [] _hand_ranks;
}

void Evaluator::Init(Handle<Object> exports)
{
    Local<FunctionTemplate> tpl = FunctionTemplate::New(New);

    tpl->SetClassName(String::NewSymbol("Evaluator"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    tpl->PrototypeTemplate()->Set(String::NewSymbol("Load"), FunctionTemplate::New(Load)->GetFunction());
    tpl->PrototypeTemplate()->Set(String::NewSymbol("Verify"), FunctionTemplate::New(Verify)->GetFunction());

    Persistent<Function> constructor = Persistent<Function>::New(tpl->GetFunction());

    exports->Set(String::NewSymbol("Evaluator"), constructor);
}

Handle<Value> Evaluator::New(const Arguments &args)
{
    HandleScope scope;

    Evaluator *obj = new Evaluator();

    obj->Wrap(args.This());

    return args.This();
}

Handle<Value> Evaluator::Verify(const Arguments &args)
{
    HandleScope scope;

    Evaluator *obj = ObjectWrap::Unwrap<Evaluator>(args.This());

    int c0, c1, c2, c3, c4, c5, c6, u0, u1, u2, u3, u4, u5, hand_equivalencies[10];
    Local<Array> result = Array::New(9);

    memset(hand_equivalencies, 0, sizeof(hand_equivalencies));

    for (c0 = 1; c0 < 53; ++c0) {
        u0 = obj->_hand_ranks[53 + c0];

        for (c1 = c0 + 1; c1 < 53; ++c1) {
            u1 = obj->_hand_ranks[u0 + c1];

            for (c2 = c1 + 1; c2 < 53; ++c2) {
                u2 = obj->_hand_ranks[u1 + c2];

                for (c3 = c2 + 1; c3 < 53; ++c3) {
                    u3 = obj->_hand_ranks[u2 + c3];

                    for (c4 = c3 + 1; c4 < 53; ++c4) {
                        u4 = obj->_hand_ranks[u3 + c4];

                        for (c5 = c4 + 1; c5 < 53; ++c5) {
                            u5 = obj->_hand_ranks[u4 + c5];

                            for (c6 = c5 + 1; c6 < 53; ++c6)
                                ++hand_equivalencies[obj->_hand_ranks[u5 + c6] >> 12];
                        }
                    }
                }
            }
        }
    }

    for (int i = 0; i < 9; i++)
        result->Set(i, Local<Value>::New(Number::New(hand_equivalencies[i + 1])));

    return scope.Close(Local<Value>::New(result));
}

Handle<Value> Evaluator::Load(const Arguments& args)
{
    HandleScope scope;

    Evaluator *obj = ObjectWrap::Unwrap<Evaluator>(args.This());

    std::ifstream i_file;
    i_file.open("/var/projects/poker-eval/handranks.dat", std::ios::in | std::ios::binary);

    if (!i_file.fail()) {
        i_file.seekg(0, std::ios::beg);
        i_file.read(reinterpret_cast<char *>(obj->_hand_ranks), sizeof(int) * 32487834);
        i_file.close();

        return scope.Close(Number::New(1));
    }

    int64_t id;
    int n_id, card = 0, id_slot;

    memset(obj->_hand_ranks, 0, sizeof(int) * 32487834);

    obj->_ids = new int64_t[612978];

    memset(obj->_ids, 0, sizeof(int64_t) * 612978);

    for (n_id = 0; obj->_ids[n_id] || n_id == 0; ++n_id)
        for (card = 1; card < 53; ++card) {
            id = obj->GenerateID(obj->_ids[n_id], card);

            if (obj->_n_cards < 7)
                obj->SaveID(id);
        }

    for (n_id = 0; obj->_ids[n_id] || n_id == 0; ++n_id) {
        for (card = 1; card < 53; ++card) {
            id = obj->GenerateID(obj->_ids[n_id], card);

            if (obj->_n_cards < 7)
                id_slot = obj->SaveID(id) * 53 + 53;
            else
                id_slot = obj->Evaluation(id);

            obj->_max_hand_rank = n_id * 53 + card + 53;
            obj->_hand_ranks[obj->_max_hand_rank] = id_slot;
        }

        if (obj->_n_cards == 6 || obj->_n_cards == 7)
            obj->_hand_ranks[n_id * 53 + 53] = obj->Evaluation(obj->_ids[n_id]);
    }

    std::ofstream o_file;

    o_file.open("/var/projects/poker-eval/handranks.dat", std::ios::out | std::ios::binary);
    o_file.write(reinterpret_cast<char *>(obj->_hand_ranks), sizeof(int) * 32487834);
    o_file.close();

    return scope.Close(Number::New(1));
}

int64_t Evaluator::GenerateID(int64_t id, int new_card)
{
    int suit_count[5];
    int rank_count[14];
    int work_cards[8];
    int n_card;

    memset(work_cards, 0, sizeof(work_cards));
    memset(rank_count, 0, sizeof(rank_count));
    memset(suit_count, 0, sizeof(suit_count));

    for (n_card = 0; n_card < 6; ++n_card)
        work_cards[n_card + 1] = static_cast<int>((id >> (8 * n_card)) & 0xff);

    --new_card;

    work_cards[0] = (((new_card >> 2) + 1) << 4) + (new_card & 3) + 1;

    for (_n_cards = 0; work_cards[_n_cards]; ++_n_cards) {
        ++suit_count[work_cards[_n_cards] & 0xf];
        ++rank_count[(work_cards[_n_cards] >> 4) & 0xf];

        if (_n_cards)
            if (work_cards[0] == work_cards[_n_cards])
                return 0;
    }

    if (_n_cards > 4)
        for (int rank = 1; rank < 14; ++rank)
            if (rank_count[rank] > 4)
                return 0;

    int need_suited = _n_cards - 2;

    if (need_suited > 1)
        for (n_card = 0; n_card < _n_cards; ++n_card)
            if (suit_count[work_cards[n_card] & 0xf] < need_suited)
                work_cards[n_card] &= 0xf0;

    _SWAP(0, 4);
    _SWAP(1, 5);
    _SWAP(2, 6);
    _SWAP(0, 2);
    _SWAP(1, 3);
    _SWAP(4, 6);
    _SWAP(2, 4);
    _SWAP(3, 5);
    _SWAP(0, 1);
    _SWAP(2, 3);
    _SWAP(4, 5);
    _SWAP(1, 4);
    _SWAP(3, 6);
    _SWAP(1, 2);
    _SWAP(3, 4);
    _SWAP(5, 6);

    return
        static_cast<int64_t>(work_cards[0]) +
        (static_cast<int64_t>(work_cards[1]) << 8) +
        (static_cast<int64_t>(work_cards[2]) << 16) +
        (static_cast<int64_t>(work_cards[3]) << 24) +
        (static_cast<int64_t>(work_cards[4]) << 32) +
        (static_cast<int64_t>(work_cards[5]) << 40) +
        (static_cast<int64_t>(work_cards[6]) << 48);
}

int Evaluator::SaveID(int64_t id)
{
    if (id == 0)
        return 0;

    if (id >= _max_id) {
        if (id > _max_id) {
            _ids[_n_id++] = id;
            _max_id = id;
        }

        return _n_id - 1;
    }

    int low  = 0, high = _n_id - 1, hold_test;
    int64_t test_value;

    while (high - low > 1) {
        hold_test = (high + low + 1) >> 1;
        test_value = _ids[hold_test] - id;

        if (test_value > 0)
            high = hold_test;
        else if (test_value < 0) {
            low = hold_test;
        } else
            return hold_test;
    }

    memmove(&_ids[high + 1], &_ids[high], (_n_id - high) * sizeof(_ids[0]));

    _ids[high] = id;

    ++_n_id;

    return high;
}

unsigned Evaluator::Find(unsigned u)
{
    u += 0xe91aaa35;
    u ^= u >> 16;
    u += u << 8;
    u ^= u >> 4;

    return ((u + (u << 2)) >> 19) ^ GHASHADJUST[(u >> 8) & 0x1ff];
}

int Evaluator::Evaluation(int64_t id)
{
    int hand_rank = 0, work_cards[8], hold_cards[8];

    memset(work_cards, 0, sizeof(work_cards));
    memset(hold_cards, 0, sizeof(hold_cards));

    if (id) {
        int n_card, work_card, rank, suit, hold_rank, main_suit = 20, suit_iterator = 1, n_evaluated_cards = 0;
        const int primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41};

        for (n_card = 0; n_card < 7; ++n_card) {
            hold_cards[n_card] = static_cast<int>((id >> (8 * n_card)) & 0xff);

            if (hold_cards[n_card] == 0)
                break;

            ++n_evaluated_cards;

            if ((suit = hold_cards[n_card] & 0xf))
                main_suit = suit;
        }

        for (n_card = 0; n_card < n_evaluated_cards; ++n_card) {
            work_card = hold_cards[n_card];
            rank = (work_card >> 4) - 1;
            suit = work_card & 0xf;

            if (suit == 0) {
                suit = suit_iterator++;

                if (suit_iterator == 5)
                    suit_iterator = 1;

                if (suit == main_suit) {
                    suit = suit_iterator++;

                    if (suit_iterator == 5)
                        suit_iterator = 1;
                }
            }

            work_cards[n_card] = primes[rank] | (rank << 8) | (1 << (suit + 11)) | (1 << (16 + rank));
        }

        switch (n_evaluated_cards) {
            case 5: hold_rank = Evaluate5Hand(work_cards); break;
            case 6: hold_rank = Evaluate6Hand(work_cards); break;
            case 7: hold_rank = Evaluate7Hand(work_cards); break;
            default: return 0; // Just to suppress warning...
        }

        hand_rank = 7463 - hold_rank;

        if (hand_rank < 1278)
            hand_rank = hand_rank - 0 + 4096 * 1;
        else if (hand_rank < 4138)
            hand_rank = hand_rank - 1277 + 4096 * 2;
        else if (hand_rank < 4996)
            hand_rank = hand_rank - 4137 + 4096 * 3;
        else if (hand_rank < 5854)
            hand_rank = hand_rank - 4995 + 4096 * 4;
        else if (hand_rank < 5864)
            hand_rank = hand_rank - 5853 + 4096 * 5;
        else if (hand_rank < 7141)
            hand_rank = hand_rank - 5863 + 4096 * 6;
        else if (hand_rank < 7297)
            hand_rank = hand_rank - 7140 + 4096 * 7;
        else if (hand_rank < 7453)
            hand_rank = hand_rank - 7296 + 4096 * 8;
        else
            hand_rank = hand_rank - 7452 + 4096 * 9;
    }

    return hand_rank;
}

int Evaluator::Evaluate5Hand(int *hand)
{
    int q = (hand[0] | hand[1] | hand[2] | hand[3] | hand[4]) >> 16;

    if (hand[0] & hand[1] & hand[2] & hand[3] & hand[4] & 0xf000)
        return GFLUSHES[q];
    else {
        short s;

        if ((s = GUNIQUE[q]))
            return s;
        else
            return GHASHVALUES[
                Find((hand[0] & 0xff) *
                (hand[1] & 0xff) *
                (hand[2] & 0xff) *
                (hand[3] & 0xff) *
                (hand[4] & 0xff))];
    }
}

int Evaluator::Evaluate6Hand(int *hand)
{
    int best = 9999, t_hand[8], q;

    SWAP(t_hand, hand)

    PERMUTATION6(0)
    PERMUTATION6(1)
    PERMUTATION6(2)
    PERMUTATION6(3)
    PERMUTATION6(4)
    PERMUTATION6(5)

    SWAP(hand, t_hand)

    return best;
}

int Evaluator::Evaluate7Hand(int *hand)
{
    int best = 9999, t_hand[8], q;

    SWAP(t_hand, hand)

    PERMUTATION7( 0)
    PERMUTATION7( 1)
    PERMUTATION7( 2)
    PERMUTATION7( 3)
    PERMUTATION7( 4)
    PERMUTATION7( 5)
    PERMUTATION7( 6)
    PERMUTATION7( 7)
    PERMUTATION7( 8)
    PERMUTATION7( 9)
    PERMUTATION7(10)
    PERMUTATION7(11)
    PERMUTATION7(12)
    PERMUTATION7(13)
    PERMUTATION7(14)
    PERMUTATION7(15)
    PERMUTATION7(16)
    PERMUTATION7(17)
    PERMUTATION7(18)
    PERMUTATION7(19)
    PERMUTATION7(20)

    SWAP(hand, t_hand)

    return best;
}


